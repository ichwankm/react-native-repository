import React, {useState, useEffect} from 'react';
import {View,  ScrollView} from 'react-native';
import BasicJavascript from './Pages/BasicJavascript';
import CallAPIAxios from './Pages/CallAPIAxios';
import CallAPIVanilla from './Pages/CallAPIVanilla';
import Communication from './Pages/Communication';
import FlexBox from './Pages/FlexBox';
import LocalAPI from './Pages/LocalAPI';
import Position from './Pages/Position';
import PropsDinamis from './Pages/PropsDinamis';
import ReactNativeSvg from './Pages/ReactNativeSvg';
import SampleComponent from './Pages/SampleComponent';
import StateDinamis from './Pages/StateDinamis';
import StylingComponent from './Pages/StylingComponent';

function App() {
  const [isShow, SetIsShow] = useState(true);
  useEffect(() => {
    setTimeout(() => {
      SetIsShow(false);
    }, 6000);
  }, []);
  return (
    <View>
      <ScrollView>
        {/* <SampleComponent/> */}
        {/* <StylingComponent/> */}
        {/* {<FlexBox/>} */}
        {/* <Position/> */}
        {/* <PropsDinamis/> */}
        {/* <StateDinamis/> */}
        {/* <Communication/> */}
        {/* <BasicJavascript/> */}
        {/* <ReactNativeSvg/> */}
        {/* <CallAPIVanilla/> */}
        {/* <CallAPIAxios /> */}
        <LocalAPI />
      </ScrollView>
    </View>
  )
}

export default App;