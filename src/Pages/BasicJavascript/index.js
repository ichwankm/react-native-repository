import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

function BasicJavascript() {
    const nama = 'Ichwan Kurnia M.';
    let usia = 24;

    const hewanPeliharaan = {
        nama: 'Miaw',
        jenis: 'Kucing',
        usia: 2,
        apakahHewanLokal: true,
        warna: 'kuning',
        orangTua: {
            jantan: 'Kaisar',
            betina: 'Kuin'
        }
    }

    const Array = ['Ichwan', 'Kurnia', 'Meidi', 'Andri', 'Anto']

    function sapaHewan(objectHewan) {
        // let hasilSapa = '';
        // if(objectHewan.nama === 'Miaw'){
        //     hasilSapa = 'Hallo Miaw, Apa Kabar?';
        // }
        // else{
        //     hasilSapa = 'Ini Hewan Siapa?'
        // }

        return objectHewan.nama === "Miaw" ? 'Hallo Miaw, Apa Kabar?' : 'Ini Hewan Siapa?'; 
    }

    return (
        <View style={styles.container}>
            <Text style={styles.textTitle}>
                Materi Basic Javascript di React Native
            </Text>
            <Text>Name: {nama}</Text>
            <Text>Usia: {usia}</Text>
            <Text>{sapaHewan(hewanPeliharaan)}</Text>
            {Array.map((i) => <Text>{i}</Text>)}
        </View>
    )
}

export default BasicJavascript

const styles = StyleSheet.create({
    container: {padding: 20},
    textTitle: {textAlign: 'center'}
})
