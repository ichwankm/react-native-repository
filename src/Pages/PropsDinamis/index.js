import React from 'react';
import {Image, ScrollView, StyleSheet, Text, View} from 'react-native';

function Story(props) {
    return(
        <View style={{alignItems: 'center', marginRight: 20}}>
           <Image source={{uri: props.Image}}
            style={{width: 70, height: 70, borderRadius: 70/2}}
            />
            <Text style={{maxWidth: 50, textAlign: 'center'}}>{props.title}</Text> 
        </View>
    );    
};

function PropsDinamis() {
    return(
        <View>
            <Text>Materi Component Dinamis dengan Props</Text>
            <ScrollView horizontal>
                <View style={{flexDirection: 'row'}}>
                    <Story 
                        Image="https://scontent-sin6-1.cdninstagram.com/v/t51.2885-19/s150x150/46323573_371732543589696_3125944860943581184_n.jpg?_nc_ht=scontent-sin6-1.cdninstagram.com&_nc_cat=100&_nc_ohc=MSfF9fEM83AAX_OqCWu&oh=ac65b52ee6f18b5d0548ad5517c38e54&oe=5FCCEE67" 
                        title="Ichwan Kurnia"
                    />
                    <Story 
                        Image="https://scontent-sin6-2.cdninstagram.com/v/t51.2885-19/s150x150/123147129_184827726575540_612279987506255615_n.jpg?_nc_ht=scontent-sin6-2.cdninstagram.com&_nc_cat=1&_nc_ohc=ZZCU0VbbLp0AX-qsJ9O&oh=ddcffb082879112e987978995a44e69c&oe=5FCE6C6D" 
                        title="Arsenal"
                    />
                    <Story 
                        Image="https://scontent-sin6-2.cdninstagram.com/v/t51.2885-19/s150x150/44329317_268583430479565_454483638147350528_n.jpg?_nc_ht=scontent-sin6-2.cdninstagram.com&_nc_cat=109&_nc_ohc=JRgQx1DS-vwAX8M5qE8&oh=a8721c54b1cafa6667ea3fa3e899a570&oe=5FCB81B0" 
                        title="Elon Musk"
                    />
                    <Story 
                        Image="https://scontent-sin6-2.cdninstagram.com/v/t51.2885-19/s150x150/110606554_274256377193351_7760278100826446941_n.jpg?_nc_ht=scontent-sin6-2.cdninstagram.com&_nc_cat=1&_nc_ohc=InxXJvOZVnoAX9fo5sA&oh=1360a050be4de615a0df78fc184fe602&oe=5FCF26D6" 
                        title="Taylor Swift"
                    />
                    <Story 
                        Image="https://scontent-sin6-2.cdninstagram.com/v/t51.2885-19/s150x150/26429265_1801356139896204_7973709430447407104_n.jpg?_nc_ht=scontent-sin6-2.cdninstagram.com&_nc_cat=1&_nc_ohc=pQm7nSAuuQEAX-HpSB0&oh=a915eb306e6c0e2fd1492e7f9ecfebc0&oe=5FCB8984" 
                        title="Gordon Ramsey"
                    />
                    <Story 
                        Image="https://scontent-sin6-2.cdninstagram.com/v/t51.2885-19/s150x150/61900634_2143548305740826_3680802202626031616_n.jpg?_nc_ht=scontent-sin6-2.cdninstagram.com&_nc_cat=1&_nc_ohc=pQX5IpqyNzwAX-2Q67Q&oh=62458acfa43121fddc6fcca5e11b6cb2&oe=5FCDD833" 
                        title="Alex Morgan"
                    />
                </View>
            </ScrollView>
        </View>
    );
};

export default PropsDinamis;

const styles = StyleSheet.create({})