import React, {Component} from 'react';
import {View, Text, TextInput, Image} from 'react-native';

function SampleComponent() {
    return(
      <View>
        <View style={{width: 80, height: 80, backgroundColor: '#0abde3'}} />
        <Text>Ichwan</Text>
        <Text>Kurnia</Text>
        <TwoApps/>
        <Text>Meidiandrianto</Text>
        <Photo />
        <TextInput style={{borderWidth: 1}} />
        <BoxGreen/>
        <Profile/>
      </View>
    )
  }
  
  function TwoApps() {
    return <Text>Andri</Text>
  }
  
  function Photo() {
    return <Image 
            source={require('../../Assets/Image/unnamed.png')}
            style={{width: 100, height: 100}}/>
  }
  
  class BoxGreen extends Component {
    render() {
      return <Text>Ini Component dari Class</Text>
    }
  }
  
  class Profile extends Component {
    render() {
      return (
        <View>
          <Image source={require('../../Assets/Image/chelsea.jpg')} style={{width: 100, height: 100, borderRadius: 50}}>
          </Image>
          <Text style={{color: 'blue', fontSize: 24}}>Ini Logo Chelsea FC</Text>
        </View>
      )
    }
  }

  export default SampleComponent;