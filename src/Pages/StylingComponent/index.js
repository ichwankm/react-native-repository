import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import unnamed from '../../Assets/Image/unnamed.png'

function StylingComponent() {
    return(
      <View>
        <Text style={styles.text}>Styling Component</Text>
        <View style={{width: 100, height: 100, backgroundColor: '#0abde3', borderWidth: 2, borderColor: '#5f27cd', marginTop: 20, marginLeft: 20}} />
        <View style={{padding: 12, backgroundColor: '#F2F2F2', width: 200, borderRadius: 8}}>
          <Image source={unnamed} style={{width:188, height:107, borderRadius: 8}} ></Image>
          <Text style={{fontSize:18, fontWeight: 'bold', marginTop: 16, marginLeft: 26}}>Wallpaper Naga</Text>
          <Text style={{fontSize:12, fontWeight: 'bold', color: '#f2994a', marginTop: 12, marginLeft: 45}}>Rp. 25.000,00</Text>
          <Text style={{fontSize:12, fontWeight: '300', marginTop: 12, marginLeft: 15, marginBottom: 12, color: 'blue'}}>www.DragonWallpaper.com</Text>
          <View style={{backgroundColor: '#6fcf97', paddingVertical: 6, borderRadius: 25}}>
            <Text style={{fontSize: 14, fontWeight: '600', color:'white', textAlign: 'center'}}>BELI</Text>
        </View>
        </View>
      </View>
    )
  }
  
  const styles = StyleSheet.create({
    text: {
      fontSize: 18,
      fontWeight: 'bold',
      color: '#10ac84',
      marginLeft: 20,
      marginTop: 40
    }
  })

  export default StylingComponent;