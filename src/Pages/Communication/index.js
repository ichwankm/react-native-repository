import React, { useState } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import Cart from '../../Components/Cart'
import Product from '../../Components/Product'

export default function Communication() {
    const [totalProduct, setTotalProduct] = useState(0);
    return (
        <View style={styles.container}>
            <Text style={styles.textTitle}>Materi Communication antar component</Text>
            <Cart quantity={totalProduct}/>
            <Product onButtonPress={() => setTotalProduct(totalProduct + 1)}/>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {padding: 20},
    textTitle: {textAlign: 'center'}
})
