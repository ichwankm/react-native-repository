import React, { Component, useState } from 'react'
import { View, Text, StyleSheet, Button } from 'react-native'

export default function StateDinamis() {
    return (
        <View style={styles.wrapper}>
            <Text style={styles.textTitle}>Materi Component Dinamis dengan State</Text>
            <Text style={styles.titleSelection}>Functional Component (Hooks)</Text>
            <Counter/>
            <Counter/>
            <Text style={styles.titleSelection}>Class Component</Text>
            <CounterClass />
            <CounterClass />
        </View>
    )
}

class CounterClass extends Component {
    state = {
        number: 0
    }
    render(){
        return(
        <View>
            <Text>{this.state.number}</Text>
            <Button title="Tambah" onPress={() => this.setState({number: this.state.number + 1})} />
        </View>
        )
    }
}

function Counter() {
    const [number, setNumber] = useState(0)
    return (
        <View>
            <Text>{number}</Text>
            <Button title="Tambah" onPress={() => setNumber(number + 1)} />
        </View>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        padding: 20
    },
    textTitle: {
        textAlign: 'center'
    },
    titleSelection: {
        marginTop: 20
    }
});
