import React from 'react'
import { Button, StyleSheet, Text, TextInput, View, Image } from 'react-native'

function Item() {
    return (
        <View style={styles.itemContainer}>
        <Image source={require('../../Assets/Image/avataaars.png')} style={styles.avatar} />
        <View style={styles.desc}>
            <Text style={styles.descName}>Nama Lengkap</Text>
            <Text style={styles.descEmail}>Email</Text>
            <Text style={styles.descBidang}>Bidang</Text>
        </View>
    </View>
    )
}

const LocalAPI = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.textTitle}>Local API (JSON Server)</Text>
            <Text>Masukan Anggota</Text>
            <TextInput placeholder="Nama Lengkap" style={styles.input}/>
            <TextInput placeholder="Email" style={styles.input} />
            <TextInput placeholder="Bidang" style={styles.input} />
            <Button title="Simpan" />
            <View style={styles.line} />
            <Item />    
        </View>
    )
}

export default LocalAPI

const styles = StyleSheet.create({
    container: {padding: 20},
    textTitle: {textAlign: 'center', marginBottom: 20},
    line: {height: 2, backgroundColor: 'black', marginVertical: 20},
    input: {borderWidth: 1, marginBottom: 12, borderRadius: 25, paddingHorizontal: 18},
    avatar: {width: 100, height: 100, borderRadius: 100},
    itemContainer: {flexDirection: 'row'},
    desc: {marginLeft: 18},
    descName: {fontSize: 20, fontWeight: "bold"},
    descEmail: {fontSize: 16},
    descBidang: {fontSize: 12, marginTop: 8}
})
