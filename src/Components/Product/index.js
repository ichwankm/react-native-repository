import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import unnamed from '../../Assets/Image/unnamed.png'

function Product (props) {
    return (
        <View>
            <View style={styles.wrapper}>
                <Image source={unnamed} style={styles.imageProduct} ></Image>
                <Text style={styles.productName}>Wallpaper Naga</Text>
                <Text style={styles.productPrice}>Rp. 25.000,00</Text>
                <Text style={styles.location}>www.DragonWallpaper.com</Text>
                <TouchableOpacity onPress={props.onButtonPress}>
                    <View style={styles.buttonWrapper}>
                        <Text style={styles.buttonText}>BELI</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default Product

const styles = StyleSheet.create({
    wrapper: {padding: 12, backgroundColor: '#F2F2F2', width: 200, borderRadius: 8},
    imageProduct: {width:188, height:107, borderRadius: 8},
    productName: {fontSize:18, fontWeight: 'bold', marginTop: 16, marginLeft: 26},
    productPrice: {fontSize:12, fontWeight: 'bold', color: '#f2994a', marginTop: 12, marginLeft: 45},
    location: {fontSize:12, fontWeight: '300', marginTop: 12, marginLeft: 15, marginBottom: 12, color: 'blue'},
    buttonWrapper: {backgroundColor: '#6fcf97', paddingVertical: 6, borderRadius: 25},
    buttonText: {fontSize: 14, fontWeight: '600', color:'white', textAlign: 'center'}
})
